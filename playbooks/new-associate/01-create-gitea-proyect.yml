---
- name: Create new Project
  hosts: all
  gather_facts: false

  tasks:
    - name: Generate a random password
      ansible.builtin.set_stats:
        data:
          new_user_password: "{{ lookup('ansible.builtin.password', '/dev/null', chars=['ascii_lowercase', 'digits'], length=8) }}"
      register: new_user_password

    - name: "Create {{ new_user_username }} user"
      ansible.builtin.uri:
        url: "http://gitea.{{ ocp_ingress_domain }}/user/sign_up"
        method: POST
        headers:
          Content-Type: application/json
        body: >
          { 
           "username": "{{ new_user_username }}", 
           "password": "{{ new_user_password.ansible_stats.data.new_user_password }}", 
           "retype": "{{ new_user_password.ansible_stats.data.new_user_password }}", 
           "email": "{{ new_user_email }}", 
           "send_notify": false 
          }
        body_format: json
        validate_certs: false
        status_code: 
          - 200
      register: _result
      until: _result.status == 200
      retries: 720 
      delay: 5

    - name: "Create {{ new_project_name }} repository"
      ansible.builtin.uri:
        url: "http://gitea.{{ ocp_ingress_domain }}/api/v1/user/repos"
        user: "{{ new_user_username }}"
        password: "{{ new_user_password.ansible_stats.data.new_user_password }}"
        method: POST
        headers:
          Content-Type: application/json
        body: > 
          { 
           "auto_init": true, 
           "default_branch": "master", 
           "gitignores": "VisualStudioCode", 
           "name": "{{ new_project_name }}", 
           "private": false, 
           "trust_model": "default" 
          }
        body_format: json
        validate_certs: false
        force_basic_auth: yes
        status_code:
          - 201
          - 409
        return_content: true

    - name: "Clone {{ new_project_name }} repository"
      ansible.builtin.shell: git clone http://{{ new_user_username }}:{{ new_user_password.ansible_stats.data.new_user_password }}@gitea.{{ ocp_ingress_domain }}/{{ new_user_username }}/{{ new_project_name }}.git {{ new_project_name }}-tmp
      args:
        chdir: /tmp

    - name: "Template dev.yml file to {{ new_project_name }} repository"
      ansible.builtin.template:
        src: ../../templates/dev.yml.j2
        dest: /tmp/{{ new_project_name }}-tmp/dev.yml

    - name: "Template README.md file to {{ new_project_name }} repository"
      ansible.builtin.template:
        src: ../../templates/README.md.j2
        dest: /tmp/{{ new_project_name }}-tmp/README.md

    - name: Git Push to master
      ansible.builtin.shell: |
        git config user.email "{{ new_user_email }}"
        git config user.name "{{ new_user_username }}"
        git add .
        git commit -m "Pushing dev.yml and README.md"
        git push
      args:
        chdir: /tmp/{{ new_project_name }}-tmp
